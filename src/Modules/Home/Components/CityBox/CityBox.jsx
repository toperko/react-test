import React from 'react';
import styled from 'styled-components'

class CityBox extends React.Component {


    render() {
        const Box = styled.section`
            padding: 10px;
            height: 200px;
            line-height: 200px;
            background: ${props => props.background ? `url(${props.background}) no-repeat center center` : `#b8b8b8`};
            background-size: cover;
            text-align: center;
        `;

        const Title = styled.h1`
            display: inline-block;
            vertical-align: middle;
            line-height: normal;
            font-size: 2.5em;
            font-weight: bold;
            
            color: white;
            text-shadow: 0 0 5px #000000;
        `;

        return <Box background={this.props.background}>
            <Title>{this.props.name}</Title>
        </Box>
    }
}

export default CityBox;