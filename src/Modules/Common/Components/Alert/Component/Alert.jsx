import React from 'react';
import styled from 'styled-components';

class AlertClass extends React.Component {
    render() {
        const Alert = styled.div`
            background: #d0d0d0;
            font-size: 1em;
            padding: 10px;
            margin: 10px auto;
            border-radius: 5px;
            color: #3f6600;
        `;

        return <Alert>{this.props.message}</Alert>
    }
}

export {AlertClass as Alert};