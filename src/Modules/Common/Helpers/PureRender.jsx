import React from 'react';
import PropTypes from 'prop-types';
import shouldPureComponentUpdate from 'react-pure-render/function';

export default class PureRenderer extends React.Component {
    static propTypes = {
        render: PropTypes.func
    };

    shouldComponentUpdate = shouldPureComponentUpdate;

    constructor(props) {
        super(props);
    }

    render() {
        const {render, ...other} = this.props;
        return render(other);
    }
}