const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const lessToJs = require('less-vars-to-js');
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, './src/Themes/vars.less'), 'utf8'));

const htmlWebpackPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});

const miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "[id].css"
});

module.exports = {
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].js',
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.svg'],
        alias: {
            Common: path.resolve(__dirname, 'src/Modules/Common'),
            Modules: path.resolve(__dirname, 'src/Modules'),
            Themes: path.resolve(__dirname, 'src/Themes'),
            Public: path.resolve(__dirname, 'public'),
            Config: path.resolve(__dirname, 'config.js'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$|\.jsx$/,
                use: {
                    loader: "babel-loader",
                    options: {
                        plugins: [
                            ['import', {libraryName: "antd", style: true}]
                        ]
                    }
                }
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: [{
                    loader: MiniCssExtractPlugin.loader
                }, {
                    loader: "css-loader",
                    options: {
                        modules: true
                    }
                }, {
                    loader: "less-loader",
                    options: {
                        javascriptEnabled: true,
                        modifyVars: themeVariables
                    }
                }],
            },
            {
                test: /\.less$/,
                exclude: /src/,
                use: [{
                    loader: MiniCssExtractPlugin.loader
                }, {
                    loader: "css-loader",
                }, {
                    loader: "less-loader",
                    options: {
                        javascriptEnabled: true,
                        modifyVars: themeVariables
                    }
                }],
            },
            {
                test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
                use: 'file-loader?name=[name].[ext]'
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ],
            },
        ],
    },
    optimization: {
        runtimeChunk: 'single',
        minimizer: [
            new TerserPlugin(),
            new OptimizeCSSAssetsPlugin()
        ],
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 100000,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        // get the name. E.g. node_modules/packageName/not/this/part.js
                        // or node_modules/packageName
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        // npm package names are URL-safe, but some servers don't like @ symbols
                        return `npm.${packageName.replace('@', '')}`;
                    },
                },
            },
        }
    },
    plugins: [
        htmlWebpackPlugin,
        miniCssExtractPlugin,
        new webpack.HashedModuleIdsPlugin()
    ],
};
