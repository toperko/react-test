import React from 'react';
import {connect} from 'react-redux';
import {Button, Layout, Form, Upload, Icon, Modal} from 'antd';
import {api} from 'Common/Api';

import config from 'Config';
import {Link} from "react-router-dom";

class PicturesWall extends React.Component {

    state = {
        previewVisible: false,
        previewImage: '',
        fileList: [],
        placeId: 1
    };

    constructor(props) {
        super(props);
        this.state.fileList = props.fileList;
        this.state.placeId = props.placeId;
    }

    handleCancel = () => this.setState({ previewVisible: false });

    handleRemove = async file => {
        api.delete(file.url);
    };

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    };

    handleChange = ({ fileList }) => this.setState({ fileList });

    render() {
        const { previewVisible, previewImage, fileList } = this.state;
        const uploadButton = (
            <Button>
                <Icon type="upload" /> Upload
            </Button>
        );

        return (
            <div className="clearfix">
                <Upload
                    action={config.apiUrl + "/image/" + this.state.placeId}
                    listType="picture"
                    fileList={fileList}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                    onRemove={this.handleRemove}
                >
                    {fileList.length >= 8 ? null : uploadButton}
                </Upload>
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                </Modal>
            </div>
        );
    }
}

class AddPhotosPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            place: null,
            images: [],
        };
    }

    componentDidMount() {
        this.setState({ placeId: this.props.match.params.id});
        api.get('/place/' + this.props.match.params.id).then(res => {
            const place = res.data;
            let r = /\d+$/;
            const photos = place.photos.map(function (item) {
                return {
                    uid: item.match(r).toString(),
                    name: 'image.png',
                    status: 'done',
                    url: item,
                };
            });

            this.setState({
                place: place,
                photos: photos
            });
        });
    }

    render() {
        const {Content} = Layout;
        const {place} = this.state;

        return !!place ?
            <Content>
                <h3>Zdjęcia dla: {place.name}</h3>
                <PicturesWall fileList={this.state.photos} placeId={place.id} />
                <Link to="/admin" type="secondary">
                    <Button>
                        Dalej
                    </Button>
                </Link>
            </Content> : <div>Loading</div>;
    }
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

function mapStateToProps(state) {
    const {authentication} = state;
    const {user} = authentication;
    return {
        user
    };
}

const connectedPhotosPage = connect(mapStateToProps)(Form.create({name: 'normal_'})(AddPhotosPage));
const AddPhotos = connect(mapStateToProps)(connectedPhotosPage);
export {AddPhotos};
