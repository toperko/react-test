import axios from 'axios';
import config from 'Config'
import authHeader from 'Modules/User/Helpers/AuthHeader'

const headers = {
    'Content-Type': 'application/json',
};

let axiosApi = axios.create({
    baseURL: config.apiUrl,
    headers: {...headers, ...authHeader() },
});

export {axiosApi as api};