import {InlineHeader} from 'Common/Components/InlineHeader/InlineHeader';
import {Map} from 'Common/Components/Map';
import {connect} from 'react-redux';
import {Home} from 'Modules/Home';
import config from 'Config';
import React from 'react';
import {Layout} from 'antd';
import {Alert} from 'Common/Components/Alert';

const {Content, Footer} = Layout;

class DefaultContainerWithMap extends React.Component {

    render() {
        const { alert, user } = this.props;
        return (
            <div>
                <InlineHeader/>
                <Content style={{padding: '0', marginTop: 64}}>
                    <Map/>
                    {alert.message &&
                    <Alert type={alert.type} message={alert.message} />

                    }
                    <div style={{padding: 24, minHeight: 380}}>
                        <Home/>
                    </div>
                </Content>
                <Footer style={{textAlign: 'center'}}>
                    {config.copyright}
                </Footer>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {alert, authentication} = state;
    const { user } = authentication;
    return {
        alert,
        user
    };
}

const connectedDefaultContainer = connect(mapStateToProps)(DefaultContainerWithMap);
export {connectedDefaultContainer as DefaultContainerWithMap};

