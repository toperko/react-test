import React from 'react'
import {Carousel, Col, Row, Typography, Icon, Button, Form, Input, DatePicker, Progress} from 'antd';
import styled from 'styled-components';
import {Image} from 'react-bootstrap';
import config from 'Config';
import Marker from 'Common/Components/Map/Marker';
import GoogleMapReact from 'google-map-react';
import {api} from 'Common/Api';
import {history} from "Common/Helpers";

const inputParsers = {
    date(input) {
        const [month, day, year] = input.split('/');
        return `${year}-${month}-${day}`;
    },
    uppercase(input) {
        return input.toUpperCase();
    },
    number(input) {
        return parseFloat(input);
    },
};

const { RangePicker } = DatePicker;

class Place extends React.Component {

    state = {
        place: null,
        from: null,
        to: null,
        moments: [],
    };

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        api.get('/place/' + this.props.match.params.id).then(res => {
            const place = res.data;
            this.setState({ place: place});
        });
    }

    onChange(date, dateString) {
        this.setState({
            from: dateString[0],
            to: dateString[1],
            moments: date,
        });
    }


    handleSubmit(event) {
        event.preventDefault();
        const form = event.target;
        const data = new FormData(form);

        for (let name of data.keys()) {
            const input = form.elements[name];
            const parserName = input.dataset.parse;

            if (parserName) {
                const parser = inputParsers[parserName];
                const parsedValue = parser(data.get(name));
                data.set(name, parsedValue);
            }
        }

        data.set('from', this.state.from);
        data.set('to', this.state.to);

        api.post('place/ask/' + this.state.place.id, {
            email: data.get('email'),
            message: data.get('message'),
            dateFrom: this.state.from,
            dateTo: this.state.to,
        }).then(
            history.push('/')
        );
    }

    render() {
        const {place} = this.state;
        const { Title } = Typography;

        const PlaceTitle = styled(Title)`
            padding: 10px;
            text-align: center;
            color: #494949 !important;
        `;

        const PhotoImage = styled(Image)`
            width: 100%;
            height: auto;
        `;

        const Details = styled(Col)`
            padding: 20px;
        `;

        const PlaceMapBox = styled(Col)`
            height: 350px;
            width: 100%;
        `;

        const ReservationButton = styled(Button)`
            margin-top: 20px;
        `;

        const ContactForm = styled(Form)`
            margin: 40px;
        `;

        const ContactFormHeader = styled.h2`
            text-align: center;
            font-size: 2em;
            margin-top: 40px;
            margin-bottom: -40px;
        `;

        const RateProgress = styled(Progress)`
            margin: 10px;
        `;


        return !!place ?
        <Row>
            <Col md={{span: 12}}>
                <Carousel autoplay>
                    {place.photos.map((item) => {
                        return <div><PhotoImage src={item}/></div>
                    })}
                </Carousel>
            </Col>
            <Details md={{span: 12}}>
                <PlaceTitle level={2}>{place.name}</PlaceTitle>
            </Details>
            <Details md={{span: 6}}>

                <Row>
                    <Col md={{span: 12}}>
                        <b>Oceny</b>:
                    </Col>
                    <Col md={{span: 12}}>
                        <div style={{fontSize: 14}}>
                                    <span style={{color: 'grey'}}>
                                        <b>{place.rating}{' '}</b>
                                    </span>
                            <span style={{color: 'orange'}}>
                                        {String.fromCharCode(9733).repeat(Math.floor(place.rating))}
                                    </span>
                            <span style={{color: 'lightgrey'}}>
                                    {String.fromCharCode(9733).repeat(5 - Math.floor(place.rating))}
                                    </span>
                        </div>
                    </Col>
                    <Col md={{span: 12}}>
                        <b>Już od</b>:
                    </Col>
                    <Col md={{span: 12}}>
                        <div style={{fontSize: 14}}>
                            <b style={{color: 'green'}}>{place.price}</b> PLN / Doba
                        </div>
                    </Col>
                    <Col md={{span: 12}}>
                        <Icon type="home" /> :
                    </Col>
                    <Col md={{span: 12}}>
                        {place.type === 'flat' ? 'Mieszkanie' : 'Dom'}
                    </Col>
                    <Col md={{span: 12}}>
                        <b>Adres</b>:
                    </Col>
                    <Col md={{span: 12}}>
                        <div>{place.address}</div>
                    </Col>
                    <Col md={{span: 12}}></Col>
                    <Col md={{span: 12}}>
                        <ReservationButton type="primary" size={'large'}><Icon type="shopping-cart" /> Rezerwacja</ReservationButton>
                    </Col>
                </Row>
            </Details>
            <Details md={{span: 6}}>
                <Title level={4}>Opis</Title>
                {place.description}
            </Details>
            <Details md={{span: 12}}>
                <Row>
                    <Col md={{span: 8}}>
                        <strong>Imprezy i Wydarzenia</strong>
                    </Col>
                    <Col md={{span: 8}}>
                        <strong>Rodzinny wyjazd</strong>
                    </Col>
                    <Col md={{span: 8}}>
                        <strong>W centrum kultury</strong>
                    </Col>
                </Row>
                <Row>
                    <Col md={{span: 8}}>
                        <RateProgress type="circle" percent={place.partyRate} width={80} />
                    </Col>
                    <Col md={{span: 8}}>
                        <RateProgress type="circle" percent={place.familyRate} width={80} />
                    </Col>
                    <Col md={{span: 8}}>
                        <RateProgress type="circle" percent={place.cultureRate} width={80} />
                    </Col>
                </Row>
            </Details>
            <PlaceMapBox md={{span: 24}}>
                <GoogleMapReact
                    bootstrapURLKeys={{key: config.googleApiKey}}
                    defaultCenter={{
                        lat: place.location.lat,
                        lng: place.location.lng
                    }}
                    defaultZoom={14}
                >
                        <Marker
                            key={place.id}
                            lat={place.location.lat}
                            lng={place.location.lng}
                            show={true}
                            place={place}
                        />
                </GoogleMapReact>
            </PlaceMapBox>
            <Col md={{span: 24}}>
                <ContactFormHeader>Wyślij zapytanie:</ContactFormHeader>
                <ContactForm onSubmit={this.handleSubmit}>
                    <Form.Item label="E-mail">
                        <Input name={'email'} />
                    </Form.Item>
                    <Form.Item label="wiadomość">
                        <Input name={'message'}  />
                    </Form.Item>
                    <Form.Item label="Data">
                        <RangePicker
                            onChange={this.onChange}
                            value={this.state.moments}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Wyślij
                        </Button>
                    </Form.Item>
                </ContactForm>
            </Col>
        </Row>

         : <div>Loading</div>;
    }
}

export { Place }