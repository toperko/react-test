import { userService, userConstants } from '../index';
import { alertActions } from 'Common/Components/Alert';
import { history } from 'Common/Helpers';

export const userActions = {
    login,
    logout,
    register
};

function login(email, password) {
    return dispatch => {
        dispatch(request({ email }));

        userService.login(email, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    console.log(error);
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function register(email, password, firstName, lastName, phone) {
    return dispatch => {
        dispatch(request({ email }));

        userService.register(email, password, firstName, lastName, phone)
            .then(
                user => {
                    dispatch(success('Zaloguj się.'));
                    dispatch(alertActions.success('Zaloguj się.'));
                    history.push('/login');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}
