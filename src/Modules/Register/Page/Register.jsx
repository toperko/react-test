import React from 'react';
import style from "Modules/Login/Login.less";
import {Button, Form, Icon, Input} from "antd";
import connect from "react-redux/es/connect/connect";
import {userActions} from "Modules/User";
import {Link} from "react-router-dom";
import {Alert} from 'Common/Components/Alert';

class NormalRegisterForm extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(userActions.logout());

        this.state = {
            email: '',
            firstName: '',
            lastName: '',
            password: '',
            phone: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({submitted: true});
        const {email, password, firstName, lastName, phone} = this.state;

        const {dispatch} = this.props;
        if (email && password && firstName && lastName && phone) {
            dispatch(userActions.register(email, password, firstName, lastName, phone));
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        const {alert} = this.props;
        return (
            <Form onSubmit={this.handleSubmit} className={style.loginForm}>
                <img src={'public/logo.svg'} className={style.loginLogo}/>
                {alert.message &&
                <Alert type={alert.type} message={alert.message}/>
                }
                <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [{required: true, message: 'Uzupełnij email!'}],
                    })(
                        <Input
                            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            name="email"
                            placeholder="Email"
                            type="email"
                            onChange={this.handleChange}
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('firstName', {
                        rules: [{required: true, message: 'uzupełnij imię!'}],
                    })(
                        <Input
                            name="firstName"
                            placeholder="Imię"
                            type="text"
                            onChange={this.handleChange}
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('lastName', {
                        rules: [{required: true, message: 'Uzupełnij nazwisko!'}],
                    })(
                        <Input
                            name="lastName"
                            placeholder="Nazwisko"
                            type="text"
                            onChange={this.handleChange}
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('phone', {
                        rules: [{required: true, message: 'Uzupełnij numer telefonu!'}],
                    })(
                        <Input
                            name="phone"
                            placeholder="Numer Telefonu"
                            type="phone"
                            onChange={this.handleChange}
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{required: true, message: 'Uzupełnij hasło!'}],
                    })(
                        <Input
                            prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            name="password"
                            type="password" placeholder="Hasło"
                            onChange={this.handleChange}
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className={style.loginFormButton}>
                        Zarejestruj się
                    </Button>
                    <Link to="/" type="secondary">
                        <Button className={style.loginFormButton}>
                            Strona główna
                        </Button>
                    </Link>
                </Form.Item>
            </Form>
        );
    };
}

function mapStateToProps(state) {
    const {loggingIn} = state.authentication;
    const {alert} = state;
    return {
        loggingIn,
        alert
    };
}

const connectedRegisterPage = connect(mapStateToProps)(Form.create({name: 'normal_register'})(NormalRegisterForm));
export {connectedRegisterPage as Register};
