module.exports = {
    siteName: 'Home Rent',
    copyright: 'Home Rent © 2019',
    logoPath: '/logo.svg',
    apiUrl: 'http://localhost:8080',
    apiPrefix: '/api/v1',
    googleApiKey: '{apiKey}',
    defaultMapPosition: {
        lat: 54.380142,
        lng: 18.607894
    },
    defaultMapZoom: 10
};
