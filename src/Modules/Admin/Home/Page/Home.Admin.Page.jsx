import React from 'react';
import { connect } from 'react-redux';
import {alertActions} from "Common/Components/Alert";
import {api} from "Common/Api";
import {Button, Card, Col, Row} from "antd";
import styled from "styled-components";
import {Link} from "react-router-dom";

class HomeAdminPage extends React.Component {

    constructor(props) {
        super(props);
        this.props.dispatch(alertActions.clear());
        this.state = {
            yourPlaces: [],
        };
    }

    componentDidMount() {
        api.get('/place/user/' + this.props.user.id).then(res => {
            this.setState({ yourPlaces: res.data});
        });
    }

    render() {
        const { user} = this.props;
        const {yourPlaces} = this.state;

        function ListAll() {
            return (
                <Row gutter={16}>
                    {yourPlaces.map(function (item) {
                        return MapListBox(item);
                    })}
                </Row>
            );
        }

        function MapListBox(item) {
            const {Meta} = Card;

            const PlaceCard = styled(Card)`
                margin-top: 10px;
            `;

            const CheckButton = styled(Button)`
                float: right;
                margin: 5px;
            `;

            return (
                <Col md={{span: 6}}>
                    <PlaceCard hoverable cover={<img alt="example" src={item.photos[0]}/>}>
                        <Meta title={item.name} description={
                            <div>
                                <div style={{fontSize: 14}}>
                                    <span style={{color: 'grey'}}>
                                        <b>{item.rating}{' '}</b>
                                    </span>
                                    <span style={{color: 'orange'}}>
                                        {String.fromCharCode(9733).repeat(Math.floor(item.rating))}
                                    </span>
                                    <span style={{color: 'lightgrey'}}>
                                    {String.fromCharCode(9733).repeat(5 - Math.floor(item.rating))}
                                    </span>
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    {item.type === 'flat' ? 'Mieszkanie': 'Dom'}
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    {'$'.repeat(item.price_level)}
                                </div>
                                <div style={{fontSize: 14}}>
                                    Od <b style={{color: 'green'}}>{item.price}</b> PLN / Doba
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    <Link to={`/place/${item.id}`}>
                                        <CheckButton type="primary" shape="round" icon="home">
                                            Edytuj
                                        </CheckButton>
                                    </Link>
                                    <Link to={`/admin/place/${item.id}/photos`}>
                                        <CheckButton type="primary" shape="round" icon="camera">
                                            Zdjęcia
                                        </CheckButton>
                                    </Link>
                                </div>
                            </div>
                        }/>
                    </PlaceCard></Col>
            );
        }

        return (
            <div className="container">
                <h1>Witaj {user.name} {user.surname}</h1>
                <h2>Twoje lokale:</h2>
                {ListAll()}
            </div>
        );
    }

}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomeAdminPage = connect(mapStateToProps)(HomeAdminPage);
export { connectedHomeAdminPage as HomeAdmin };