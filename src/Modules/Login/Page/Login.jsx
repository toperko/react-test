import React from 'react';
import { connect } from 'react-redux';
import {
    Form, Icon, Input, Button,
} from 'antd';
import {Link} from 'react-router-dom';
import { userActions } from 'Modules/User';
import style from './../Login.less';
import {Alert} from 'Common/Components/Alert';

class NormalLoginForm extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            email: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password } = this.state;
        const { dispatch } = this.props;
        if (email && password) {
            dispatch(userActions.login(email, password));
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { alert } = this.props;
        return (
            <Form onSubmit={this.handleSubmit} className={style.loginForm}>
                <img src={'public/logo.svg'} className={style.loginLogo}/>
                {alert.message &&
                <Alert type={alert.type} message={alert.message} />
                }
                <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Uzupełnij adres email!' }],
                    })(
                        <Input
                            prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
                            name='email'
                            placeholder='Email'
                            onChange={this.handleChange}
                            type='email'
                        />
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Uzupełnij hasło!' }],
                    })(
                        <Input
                            prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                            name='password'
                            type='password' placeholder='Hasło'
                            onChange={this.handleChange}
                        />
                    )}
                </Form.Item>
                <Form.Item>

                    <Button type='primary' htmlType='submit' className={style.loginFormButton}>
                        Zaloguj się
                    </Button>
                    <Link to='/register'>Zarejestruj się!</Link> <Link to='/' className={style.loginFormBackHome}>Wróć do strony głównej</Link>
                </Form.Item>
            </Form>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { alert } = state;
    return {
        loggingIn,
        alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(Form.create({ name: 'normal_login' })(NormalLoginForm));
export { connectedLoginPage as Login };
