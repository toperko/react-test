import React from 'react'
import {Row, Col, List, Card, Button} from 'antd';
import { Link } from "react-router-dom";
import config from "Config";
import Marker from "Common/Components/Map/Marker";
import GoogleMapReact from "google-map-react";
import styled from 'styled-components';
import InfiniteScroll from 'react-infinite-scroller';
import {api} from "Common/Api";

class Search extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            places: [],
            center: config.defaultMapPosition,
            zoom: config.defaultMapZoom
        };
    }

    componentDidMount() {
        api.get('/place/list').then(res => {
            const list = res.data;
            this.setState({ places: list});
        });
    }

    onClickMarker = (key) => {
        this.setState((state) => {
            state.places.forEach(function (el) {
                el.show = false;
            });
            const index = state.places.findIndex(e => e.id == key);
            state.places[index].show = !state.places[index].show;
            return {places: state.places};
        });
    };

    onCLickMap = () => {
        this.setState((state) => {
            state.places.forEach(function (el) {
                el.show = false;
            });
            return {places: state.places};
        });
    };

    onClickList = (key) => {
        this.setState((state) => {
            state.places.forEach(function (el) {
                el.show = false;
            });
            const index = state.places.findIndex(e => e.id == key);
            state.places[index].show = !state.places[index].show;
            return {
                places: state.places,
                zoom: 14,
                center: state.places[index].location
            };
        });
    };

    render() {
        const {places, zoom, center} = this.state;

        const Box = styled.div`
            height: calc(100vh - 135px);
            width: 100%;
            overflow: auto;
        `;

        const CheckButton = styled(Button)`
                float: right;
            `;

        function MapListBox(item) {
            const { Meta } = Card;

            return <Card hoverable cover={<img alt={item.name} src={item.photos[0]} />}>
                <Meta title={item.name} description={
                    <div>
                        <div style={{fontSize: 14}}>
                            <span style={{color: 'grey'}}>
                              {item.rating}{' '}
                            </span>
                                        <span style={{color: 'orange'}}>
                              {String.fromCharCode(9733).repeat(Math.floor(item.rating))}
                            </span>
                                        <span style={{color: 'lightgrey'}}>
                              {String.fromCharCode(9733).repeat(5 - Math.floor(item.rating))}
                            </span>
                        </div>
                        <div style={{fontSize: 14, color: 'grey'}}>
                            {item.type === 'flat' ? 'Mieszkanie': 'Dom'}
                        </div>
                        <div style={{fontSize: 14, color: 'grey'}}>
                            {'$'.repeat(item.price_level)}
                        </div>
                        <div style={{fontSize: 14}}>
                            Od <b style={{color: 'green'}}>{item.price}</b> PLN / Doba
                        </div>
                        <div style={{fontSize: 14, color: 'grey'}}>
                            <Link to={`/place/${item.id}`}>
                                <CheckButton type="primary" shape="round" icon="home">
                                    Sprawdź
                                </CheckButton>
                            </Link>
                        </div>
                    </div>
                } />
            </Card>
        }

        return (<Row>
            <Col sm={{span: 24}} md={{span: 17}}>
                <Box>
                    <GoogleMapReact
                        bootstrapURLKeys={{key: config.googleApiKey}}
                        defaultCenter={center}
                        defaultZoom={zoom}
                        onChildClick={this.onClickMarker}
                        onClick={this.onCLickMap}
                    >
                        {places.map(place =>
                            (<Marker
                                key={place.id}
                                lat={place.location.lat}
                                lng={place.location.lng}
                                show={place.show}
                                place={place}
                            />))}
                    </GoogleMapReact>
                </Box>
            </Col>
            <Col sm={{span: 24}} md={{span: 7}}>
                <Box>
                    <InfiniteScroll
                        useWindow={false}
                    >
                    <List
                        dataSource={places}

                        renderItem={item => (
                            <List.Item
                                data-key={item.id}
                                onClick={() => this.onClickList(item.id)}
                                style={{padding: 10}}

                            >
                                {MapListBox(item)}
                            </List.Item>
                        )}/>
                    </InfiniteScroll>
                </Box>
            </Col>
        </Row>);
    }
}

export {Search}