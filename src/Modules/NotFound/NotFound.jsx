import React from 'react';

export const NotFound = () => {
    return (
        <div className="container">
            <h1>No match</h1>
        </div>
    );
};