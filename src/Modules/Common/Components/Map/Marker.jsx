import styled from 'styled-components';
import React, {Fragment} from 'react';
import MapBox from './MapBox'

const Marker = (props) => {

    const Pin = styled.p`
        border-radius: 50%;
        border: 8px solid ${props => props.type === 'flat' ? "#1193ff" : "palevioletred"};
        width: 10px;
        height: 10px;
        cursor: pointer;
        zIndex: 10;
        
        &:after {
            position: absolute;
            content: '';
            width: 0;
            height: 0;
            bottom: -34px;
            left: -0px;
            border: 8px solid transparent;
            border-top: 17px solid ${props => props.type === 'flat' ? "#1193ff" : "palevioletred"};
        }
    `;

    return (
        <Fragment>
            <Pin type={props.place.type}/>
            {props.show && <MapBox place={props.place}/>}
        </Fragment>
    );
};

export default Marker;