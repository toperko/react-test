import React from 'react'
import {Link, Route, Switch} from 'react-router-dom';
import {Place} from './Place';
import {Row, Col, Card, Button} from 'antd';
import styled from 'styled-components';
import {api} from 'Common/Api';

class Places extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            places: []
        };
    }

    componentDidMount() {
        api.get('/place/list').then(res => {
            const list = res.data;
            this.setState({ places: list});
        });
    }

    render() {
        const {places} = this.state;

        function ListAll() {
            return (
                <Row gutter={16}>
                    {places.map(function (item) {
                        return MapListBox(item);
                    })}
                </Row>
            );
        }

        function MapListBox(item) {
            const {Meta} = Card;

            const PlaceCard = styled(Card)`
                margin-top: 10px;
            `;

            const CheckButton = styled(Button)`
                float: right;
            `;

            return (
                <Col md={{span: 6}}>
                    <PlaceCard hoverable cover={<img alt="example" src={item.photos[0]}/>}>
                        <Meta title={item.name} description={
                            <div>
                                <div style={{fontSize: 14}}>
                                    <span style={{color: 'grey'}}>
                                        <b>{item.rating}{' '}</b>
                                    </span>
                                    <span style={{color: 'orange'}}>
                                        {String.fromCharCode(9733).repeat(Math.floor(item.rating))}
                                    </span>
                                    <span style={{color: 'lightgrey'}}>
                                    {String.fromCharCode(9733).repeat(5 - Math.floor(item.rating))}
                                    </span>
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    {item.type === 'flat' ? 'Mieszkanie': 'Dom'}
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    {'$'.repeat(item.price_level)}
                                </div>
                                <div style={{fontSize: 14}}>
                                    Od <b style={{color: 'green'}}>{item.price}</b> PLN / Doba
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>

                                    <Link to={`/place/${item.id}`}>
                                        <CheckButton type="primary" shape="round" icon="home">
                                            Sprawdź
                                        </CheckButton>
                                    </Link>
                                </div>
                            </div>
                        }/>
                    </PlaceCard></Col>
            );
        }

        return (<Switch>
            <Route path='/place/:id' component={Place}/>
            <Route
                exact
                path='/place'
                render={ListAll}
            />
        </Switch>);
    }
}

export {Places}