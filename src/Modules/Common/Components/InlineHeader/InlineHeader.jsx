import React, {Component, Fragment} from 'react';
import {Menu, Layout, Avatar} from 'antd';
import {NavLink, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './Header.less';
import {connect} from 'react-redux';
import Logo from 'Public/logo.svg';

const {Header} = Layout;
const {SubMenu} = Menu;

class InlineHeader extends Component {

    static propTypes = {
        location: PropTypes.object.isRequired
    };

    render() {
        const {location, user} = this.props;

        function getRightContext() {
            if (typeof user !== "undefined") {
                return (
                    <SubMenu className={styles.right}
                             title={
                                 <Fragment>
                            <span style={{color: '#999', marginRight: 4}}>
                                Witaj,
                            </span>
                                     <span>{user.name} {user.surname}</span>
                                     <Avatar style={{marginLeft: 8}}
                                             shape="square" icon="user"/>
                                 </Fragment>
                             }
                    >
                        <Menu.Item key="SignOut">
                            <NavLink to="/login">Wyloguj</NavLink>
                        </Menu.Item>
                        <Menu.Item key="/admin"><NavLink to="/admin">Panel klienta</NavLink></Menu.Item>
                        <Menu.Item key="/admin/add"><NavLink to="/admin/add">Dodaj</NavLink></Menu.Item>
                    </SubMenu>
                );
            }
            return (
                <Menu.Item className={styles.right} key="/login"><NavLink to="/login">Zaloguj się</NavLink></Menu.Item>
            )
        }

        return (
            <Header>
                <div className={styles.logo}><img src={Logo}/>Home Rent</div>
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['/']}
                    selectedKeys={[location.pathname]}
                    style={{lineHeight: '64px'}}
                >
                    <Menu.Item key="/"><NavLink to="/">Start</NavLink></Menu.Item>
                    <Menu.Item key="/search"><NavLink to="/search">Szukaj</NavLink></Menu.Item>
                    <Menu.Item key="/place"><NavLink to="/place">Przeglądaj</NavLink></Menu.Item>
                    <Menu.Item key="/search/advance"><NavLink to="/search/advance">Dopasowane do Ciebie</NavLink></Menu.Item>
                    <Menu.Item key="/admin/add"><NavLink to="/admin/add">Dodaj ogłoszenie</NavLink></Menu.Item>
                    {typeof user !== "undefined" ? '' :
                        <Menu.Item key="/register"><NavLink to="/register">Zarejestruj się</NavLink></Menu.Item>}

                    {getRightContext()}
                </Menu>
            </Header>
        )
    }
}

function mapStateToProps(state) {
    const {authentication} = state;
    const {user} = authentication;
    return {
        user
    };
}

const InlineHeaderWithRouter = withRouter(InlineHeader);
const connectedInlineHeader = connect(mapStateToProps)(InlineHeaderWithRouter);

export {connectedInlineHeader as InlineHeader};
