import {InlineHeader} from 'Common/Components/InlineHeader';
import {Route} from 'react-router-dom';
import {HomeAdmin} from 'Modules/Admin/Home';
import config from 'Config';
import React from 'react';
import {Layout} from 'antd';
import {AddNewPlace} from 'Modules/Admin/AddNewPlace/AddNewPlace';
import {AddPhotos} from 'Modules/Admin/AddNewPlace/AddPhotos';

const {Content, Footer} = Layout;

export const DefaultAdminContainer = () => (
    <div>
        <InlineHeader/>
        <Content style={{padding: '0', marginTop: 64}}>
            <div style={{background: '#fff', padding: 24, minHeight: 380}}>
                <Route exact path="/admin" component={HomeAdmin}/>
                <Route path="/admin/add" component={AddNewPlace}/>
                <Route path='/admin/place/:id/photos' component={AddPhotos}/>
            </div>
        </Content>
        <Footer style={{textAlign: 'center'}}>
            {config.copyright}
        </Footer>
    </div>
);
