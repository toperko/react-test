import React from 'react';
import {connect} from 'react-redux';
import {Row, Col, Card, Button} from 'antd';
import CityBox from 'Modules/Home/Components/CityBox/CityBox';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {api} from 'Common/Api';
import {alertActions} from 'Common/Components/Alert';

class HomePage extends React.Component {

    constructor(props) {
        super(props);

        this.props.dispatch(alertActions.clear());

        this.state = {
            placesLast: [],
            bestPlaces: [],
        };
    }

    componentDidMount() {
        api.get('/place/last').then(res => {
            const last = res.data;
            this.setState({ placesLast: last});
        });
        api.get('/place/best').then(res => {
            const best = res.data;
            this.setState({ bestPlaces: best});
        });
    }

    render() {
        const {placesLast, bestPlaces} = this.state;

        function ListBest() {
            return (
                <Row gutter={8}>
                    {bestPlaces.map(function (item) {
                        return MapListBox(item);
                    })}
                </Row>
            );
        }

        function ListLastest() {
            return (
                <Row gutter={8}>
                    {placesLast.map(function (item) {
                        return MapListBox(item);
                    })}
                </Row>
            );
        }

        function MapListBox(item) {
            const {Meta} = Card;

            const PlaceCard = styled(Card)`
                margin-top: 10px;
            `;

            const CheckButton = styled(Button)`
                float: right;
            `;

            return (
                <Col md={{span: 6}}>
                    <PlaceCard hoverable cover={<img alt="example" src={item.photos[0]}/>}>
                        <Meta title={item.name} description={
                            <div>
                                <div style={{fontSize: 14}}>
                                    <span style={{color: 'grey'}}>
                                        <b>{item.rating}{' '}</b>
                                    </span>
                                    <span style={{color: 'orange'}}>
                                        {String.fromCharCode(9733).repeat(Math.floor(item.rating))}
                                    </span>
                                    <span style={{color: 'lightgrey'}}>
                                    {String.fromCharCode(9733).repeat(5 - Math.floor(item.rating))}
                                    </span>
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    {item.type === 'flat' ? 'Mieszkanie' : 'Dom'}
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>
                                    {'$'.repeat(item.price_level)}
                                </div>
                                <div style={{fontSize: 14}}>
                                    Od <b style={{color: 'green'}}>{item.price}</b> PLN / Doba
                                </div>
                                <div style={{fontSize: 14, color: 'grey'}}>

                                    <Link to={`/place/${item.id}`}>
                                        <CheckButton type="primary" shape="round" icon="home">
                                            Sprawdź
                                        </CheckButton>
                                    </Link>
                                </div>
                            </div>
                        }/>
                    </PlaceCard></Col>
            );
        }

        const Title = styled.h1`
            font-size: 2em;
            padding-top: 10px;
        `;

        return (
            <div className="container">
                <Title>Polecane dla Ciebie:</Title>
                <div>
                    <Row gutter={8}>
                        <Col sm={{span: 24}} md={{span: 6}}><CityBox name={'Gdynia'} background={'public/gdynia.jpg'}/></Col>
                        <Col sm={{span: 24}} md={{span: 6}}><CityBox name={'Sopot'}
                                                                     background={'public/sopot.jpg'}/></Col>
                        <Col sm={{span: 24}} md={{span: 6}}><CityBox name={'Gdańsk'} background={'public/gdansk.jpg'}/></Col>
                        <Col sm={{span: 24}} md={{span: 6}}><CityBox name={'Warszawa'}
                                                                     background={'public/warszawa.jpg'}/></Col>
                    </Row>
                </div>
                <Title>Ostatnio dodane:</Title>
                <div>
                    {ListLastest()}
                </div>
                <Title>Nawyżej oceniane:</Title>
                <div>
                    {ListBest()}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {users, authentication} = state;
    const {user} = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export {connectedHomePage as Home};
