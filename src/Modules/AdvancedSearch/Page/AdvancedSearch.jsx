import React from 'react'
import {Row, Col, List, Card, Button, Slider} from 'antd';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import InfiniteScroll from 'react-infinite-scroller';
import {api} from 'Common/Api';

class AdvancedSearch extends React.Component {

    state = {
        places: [],
        partyRate: 50,
        familyRate: 50,
        cultureRate: 50,
    };

    loadPlaces() {
        return api.get('/place/search', {
            params: {
                partyRate: this.state.partyRate,
                familyRate: this.state.familyRate,
                cultureRate: this.state.cultureRate,
            }
        });
    }

    componentDidMount() {
        this._asyncRequest = this.loadPlaces().then(
            res => {
                this._asyncRequest = null;
                this.setState({places: res.data});
            }
        );
    }

    componentWillUnmount() {
        if (this._asyncRequest) {
            this._asyncRequest.cancel();
        }
    }

    onChangeParty(value) {
        this.setState({ partyRate: value}, this.componentDidMount);
    }

    onChangeFamily(value) {
        this.setState({ familyRate: value}, this.componentDidMount);
    }

    onChangeCulture(value) {
        this.setState({ cultureRate: value}, this.componentDidMount);
    }

    render() {
        const {places} = this.state;
        const Box = styled.div`
            height: calc(100vh - 135px);
            width: 100%;
            overflow: auto;
        `;

        const CheckButton = styled(Button)`
                float: right;
            `;

        function MapListBox(item) {
            const { Meta } = Card;

            return <Card
                hoverable
                cover={<img alt="example" src={item.photos[0]} />}
            >
                <Meta title={item.name} description={
                    <div>
                        <div style={{fontSize: 14}}>
                            <span style={{color: 'grey'}}>
                              {item.rating}{' '}
                            </span>
                            <span style={{color: 'orange'}}>
                              {String.fromCharCode(9733).repeat(Math.floor(item.rating))}
                            </span>
                            <span style={{color: 'lightgrey'}}>
                              {String.fromCharCode(9733).repeat(5 - Math.floor(item.rating))}
                            </span>
                        </div>
                        <div style={{fontSize: 14, color: 'grey'}}>

                            {item.type === 'flat' ? 'Mieszkanie': 'Dom'}
                        </div>
                        <div style={{fontSize: 14, color: 'grey'}}>
                            {'$'.repeat(item.price_level)}
                        </div>
                        <div style={{fontSize: 14}}>
                            Od <b style={{color: 'green'}}>{item.price}</b> PLN / Doba
                        </div>
                        <div style={{fontSize: 14, color: 'grey'}}>

                            <Link to={`/place/${item.id}`}>
                                <CheckButton type="primary" shape="round" icon="home">
                                    Sprawdź
                                </CheckButton>
                            </Link>
                        </div>
                    </div>
                } />
            </Card>
        }

        const SliderBox = styled(Box)`
                padding: 10px;
            `;

        const CustomSlider = styled(Slider)`
                margin: 70px;
                margin-bottom: 120px;
            `;

        const Header = styled.h1`
                text-align: center;
                font-size: 2.8em;
            `;

        return (<Row>
            <Col sm={{span: 24}} md={{span: 12}}>
                <SliderBox>
                    <Header>Ustaw własne preferencje:</Header>
                    <CustomSlider
                        marks={{
                            0: {
                                style: {
                                    color: '#7cb305',
                                },
                                label: <strong>Cisza&nbsp;i&nbsp;wypoczynek</strong>,
                            },
                            100: {
                                style: {
                                    color: '#c41d7f',
                                },
                                label: <strong>Imprezy&nbsp;i&nbsp;Wydarzenia</strong>,
                            }
                        }}
                        included={false}
                        defaultValue={this.state.partyRate}
                        onAfterChange={this.onChangeParty.bind(this)}
                    />
                    <CustomSlider
                        marks={{
                            0: {
                                style: {
                                    color: '#531dab',
                                },
                                label: <strong>Dla&nbsp;singla</strong>,
                            },
                            100: {
                                style: {
                                    color: '#874d00',
                                },
                                label: <strong>Rodziny&nbsp;wyjazd</strong>,
                            }
                        }}
                        included={false}
                        defaultValue={this.state.familyRate}
                        onAfterChange={this.onChangeFamily.bind(this)}
                    />
                    <CustomSlider
                        marks={{
                            0: {
                                style: {
                                    color: '#d46b08',
                                },
                                label: <strong>Sport&nbsp;i&nbsp;rekreacja</strong>,
                            },
                            100: {
                                style: {
                                    color: '#ad2102',
                                },
                                label: <strong>W&nbsp;centrum&nbsp;kultury</strong>,
                            }
                        }}
                        included={false}
                        defaultValue={this.state.cultureRate}
                        onAfterChange={this.onChangeCulture.bind(this)}
                    />
                </SliderBox>
            </Col>
            <Col sm={{span: 24}} md={{span: 12}}>
                <Box>
                    <InfiniteScroll
                        useWindow={false}
                    >
                        <List
                            dataSource={places}
                            renderItem={item => (
                                <List.Item
                                    data-key={item.id}
                                    style={{padding: 10}}
                                >
                                    {MapListBox(item)}
                                </List.Item>
                            )}/>
                    </InfiniteScroll>
                </Box>
            </Col>
        </Row>);
    }
}

export {AdvancedSearch}