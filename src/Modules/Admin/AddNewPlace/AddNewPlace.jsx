import React from 'react';
import {connect} from 'react-redux';
import {Row, Col, Layout, Form, Input, Button, Select, InputNumber} from 'antd';
import GoogleMapReact from 'google-map-react';
import config from 'Config';
import styled from 'styled-components';
import {history} from 'Common/Helpers';
import {api} from 'Common/Api';
import Marker from 'Common/Components/Map/Marker';

class AddNewPlacePage extends React.Component {

    state = {
        name: '',
        type: '',
        price: 0.00,
        lat: '',
        lng: '',
        submitted: false
    };

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onMapSelect = this.onMapSelect.bind(this);
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleChangeSelect(e) {
        this.setState({type: e});
    }
    handleChangePrice(e) {
        this.setState({price: e});
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({submitted: true});

        let data = {
            name: this.state.name,
            type: this.state.type,
            address: null,
            photos: [],
            price: this.state.price,
            location: {
                lat: this.state.lat,
                lng: this.state.lng
            },
        };

        api.post('/place', data).then(handleResponse)
            .catch(function (error) {
                return handleResponse(error.response)
            })
            .then(data => {
                console.log(data.id);
                history.push('/admin/place/' + data.id + '/photos');
            })
    }

    onMapSelect(obj){
        this.props.form.setFieldsValue({
            lat: obj.lat,
            lng: obj.lng,
        });

        this.setState({
            lat: obj.lat,
            lng: obj.lng,
        });
    }

    render() {
        const {Content} = Layout;
        const {Option} = Select;
        const {getFieldDecorator} = this.props.form;

        const PlaceMapBox = styled.div`
            height: 350px;
            width: 100%;
        `;

        return (
            <Content>
                <Row>
                    <Col md={{span: 24}}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Item label="Nazwa">
                                {getFieldDecorator('name', {
                                    rules: [

                                        {
                                            required: true,
                                            message: 'Nazwa jest wymagana',
                                        },
                                    ],
                                })(<Input
                                    name="name"
                                    type="text"
                                    onChange={this.handleChange.bind(this)}
                                />)}
                            </Form.Item>
                            <Form.Item label="Typ">
                                {getFieldDecorator('type', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Typ jest wymagany',
                                        },
                                    ],
                                })(<Select
                                    name="type"
                                    onChange={this.handleChangeSelect.bind(this)}
                                >
                                    <Option name="type" value="flat">Mieszkanie</Option>
                                    <Option name="type" value="house">Dom</Option>
                                </Select>)}
                            </Form.Item>
                            <Form.Item label="Cena">
                                {getFieldDecorator('price', {
                                    rules: [

                                        {
                                            required: true,
                                            message: 'Cena jest wymagana',
                                        },
                                    ],
                                })(<InputNumber
                                    name="price"
                                    precision={2}
                                    step={0.01}
                                    onChange={this.handleChangePrice.bind(this)}
                                />)}
                            </Form.Item>
                            <PlaceMapBox>
                                <GoogleMapReact
                                    bootstrapURLKeys={{key: config.googleApiKey}}
                                    defaultCenter={config.defaultMapPosition}
                                    zoom={10}
                                    onClick={this.onMapSelect}
                                    yesIWantToUseGoogleMapApiInternals
                                >
                                    <Marker
                                        lat={this.state.lat}
                                        lng={this.state.lng}
                                        show={false}
                                        place={{type: "flat"}}
                                    />
                                </GoogleMapReact>
                            </PlaceMapBox>
                            <Form.Item label="Długość">
                                {getFieldDecorator('lat', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Koordynaty są wymagane, zaznacz na mapie.',
                                        },
                                    ],
                                })(<Input
                                    name="lat"
                                    type="text"
                                    onChange={this.handleChange.bind(this)}
                                />)}
                            </Form.Item>
                            <Form.Item label="Szerokość">
                                {getFieldDecorator('lng', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Koordynaty jest wymagane, zaznacz na mapie.',
                                        },
                                    ],
                                })(<Input
                                    name="lat"
                                    type="text"
                                    onChange={this.handleChange.bind(this)}
                                />)}
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit">
                                    Dalej
                                </Button>
                            </Form.Item>
                        </Form>
                    </Col>
                </Row>
            </Content>
        );
    }
}

function handleResponse(response) {
    console.log(response);
    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.message);

        return Promise.reject(error);
    }

    return response.data;
}

function mapStateToProps(state) {
    const {authentication} = state;
    const {user} = authentication;
    return {
        user
    };
}

const connectedAddNewPlacePage = connect(mapStateToProps)(Form.create({name: 'normal_'})(AddNewPlacePage));
const AddNewPlace = connect(mapStateToProps)(connectedAddNewPlacePage);
export {AddNewPlace};
