import {api} from 'Common/Api';

export const userService = {
    login,
    logout,
    register
};

function login(email, password) {
    return api.post('/login', {
        email: email,
        password: password
    })
        .then(handleResponse)
        .catch(function (error) {
            return handleResponse(error.response)
        })
        .then(user => {
            localStorage.setItem('user', JSON.stringify(user));

            return user;
        });
}

function register(email, password, fistName, lastName, phone) {
    return api.post('/register', {
        email: email,
        password: password,
        name: fistName,
        surname: lastName,
        phone: phone
    })
        .then(handleResponse)
        .catch(function (error) {
            return handleResponse(error.response)
        })
        .then(user => {
            localStorage.setItem('user', JSON.stringify(user));

            return user;
        });
}

function logout() {
    localStorage.removeItem('user');
}

function handleResponse(response) {
    console.log(response);
    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }

        const error = (response.data && response.data.message);
        
        return Promise.reject(error);
    }

    return response.data;
}
