import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import {Button} from 'antd';

const MapBox = (props) => {
    const {place} = props;
    const infoWindowStyle = {
        position: 'relative',
        bottom: 160,
        left: '-90px',

        width: 220,
        backgroundColor: 'white',
        boxShadow: '0 2px 7px 1px rgba(0, 0, 0, 0.3)',
        padding: 10,
        paddingBottom: 35,
        fontSize: 14,
        zIndex: 100,
    };

    const CheckButton = styled(Button)`
        float: right;
        padding-top: 10px;
    `;

    return (
        <div style={infoWindowStyle}>
            <div style={{fontSize: 16}}>
                {place.name}
            </div>
            <div style={{fontSize: 14}}>
                <span style={{color: 'grey'}}>
                  {place.rating}{' '}
                </span>
                <span style={{color: 'orange'}}>
                  {String.fromCharCode(9733).repeat(Math.floor(place.rating))}
                </span>
                <span style={{color: 'lightgrey'}}>
                  {String.fromCharCode(9733).repeat(5 - Math.floor(place.rating))}
                </span>
            </div>
            <div style={{fontSize: 14, color: 'grey'}}>
                {place.type === 'flat' ? 'Mieszkanie' : 'Dom'}
            </div>
            <div style={{fontSize: 14, color: 'grey'}}>
                {'$'.repeat(place.price_level)}
            </div>
            <div style={{fontSize: 14}}>
                Od <b style={{color: 'green'}}>{place.price}</b> PLN / Doba
            </div>
            <div style={{fontSize: 14, color: 'grey'}}>

                <Link to={`/place/${place.id}`}>
                    <CheckButton type="primary" shape="round" icon="home" size={'small'}>
                        Sprawdź
                    </CheckButton>
                </Link>
            </div>
        </div>
    );
};
export default MapBox;
