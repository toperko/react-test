import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import config from 'Config';
import Marker from './Marker';
import {api} from 'Common/Api';

export class Map extends Component {
    static defaultProps = {
        center: config.defaultMapPosition,
        zoom: config.defaultMapZoom
    };

    constructor(props) {
        super(props);

        this.state = {
            places: [],
        };
    }

    componentDidMount() {
        api.get('/place/list').then(res => {
            const list = res.data;
            this.setState({ places: list});
        });
    }

    onChildClickCallback = (key) => {
        this.setState((state) => {
            state.places.forEach(function (el) {
                el.show = false;
            });
            const index = state.places.findIndex(e => e.id == key);
            state.places[index].show = !state.places[index].show;
            return {places: state.places};
        });
    };

    handleClick = () => {
        this.setState((state) => {
            state.places.forEach(function (el) {
                el.show = false;
            });
            return {places: state.places};
        });
    };

    render() {
        const {places} = this.state;
        return (
            <div style={{height: '40vh', width: '100%'}}>
                <GoogleMapReact
                    bootstrapURLKeys={{key: config.googleApiKey}}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    onChildClick={this.onChildClickCallback}
                    onClick={this.handleClick}
                >
                    {places.map(place =>
                        (<Marker
                            key={place.id}
                            lat={place.location.lat}
                            lng={place.location.lng}
                            show={place.show}
                            place={place}
                        />))}
                </GoogleMapReact>
            </div>
        );
    }
}
