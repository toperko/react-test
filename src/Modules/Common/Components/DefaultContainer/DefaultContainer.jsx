import {InlineHeader} from 'Common/Components/InlineHeader/InlineHeader';
import {Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import {Search} from 'Modules/Search';
import {Places} from 'Modules/Place';
import {AdvancedSearch} from 'Modules/AdvancedSearch';
import config from 'Config';
import React from 'react';
import {Layout} from 'antd';
import {Alert} from 'Common/Components/Alert';
import {alertActions} from 'Common/Components/Alert';

const {Content, Footer} = Layout;

class DefaultContainer extends React.Component {
    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        dispatch(alertActions.clear());
    }

    render() {
        const {alert, user} = this.props;
        return (
            <div>
                <InlineHeader/>
                <Content style={{padding: '0', marginTop: 64}}>
                    {alert.message &&
                    <Alert type={alert.type} message={alert.message} />
                    }
                    <div>
                        <Switch>
                            <Route exact path="/search" component={Search}/>
                            <Route path="/place" component={Places}/>
                            <Route path="/search/advance" component={AdvancedSearch}/>
                        </Switch>
                    </div>
                </Content>
                <Footer style={{textAlign: 'center'}}>
                    {config.copyright}
                </Footer>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {alert, authentication} = state;
    const {user} = authentication;
    return {
        alert,
        user
    };
}

const connectedDefaultContainer = connect(mapStateToProps)(DefaultContainer);
export {connectedDefaultContainer as DefaultContainer};

