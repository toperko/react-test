import React from 'react';
import {
    Router,
    Route,
    Switch
} from 'react-router-dom';
import {connect} from 'react-redux';
import {Layout} from 'antd';
import {Login} from 'Modules/Login';
import {Register} from 'Modules/Register';
import {history} from 'Modules/Common/Helpers';
import {PrivateRoute} from 'Common/Components/PrivateRoute';
import {DefaultContainer, DefaultContainerWithMap} from 'Common/Components/DefaultContainer';
import {DefaultAdminContainer} from 'Modules/Admin/Common/Componets/DefaultAdminContainer';

class App extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Layout>
                    <Switch>
                        <Route exact path="/" component={DefaultContainerWithMap}/>
                        <Route exact path="/login" component={Login}/>
                        <Route exact path="/register" component={Register}/>
                        <Route exact path="/places/tests" component={Register}/>
                        <PrivateRoute path="/admin" component={DefaultAdminContainer}/>

                        <Route component={DefaultContainer}/>
                    </Switch>
                </Layout>
            </Router>
        );
    }
}

function mapStateToProps(state) {
    const {alert} = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export {connectedApp as App};
